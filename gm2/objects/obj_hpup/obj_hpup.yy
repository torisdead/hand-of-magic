{
    "id": "aed43a7c-cea2-4ffe-b0f5-6e6f02cc0754",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hpup",
    "eventList": [
        {
            "id": "09ed75d8-aed0-41df-b2df-0ca53743ce18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aed43a7c-cea2-4ffe-b0f5-6e6f02cc0754"
        },
        {
            "id": "a8f6932b-bf62-4186-ac9a-511627019c0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "db63e074-e648-4dd7-b166-10439aa3ea99",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aed43a7c-cea2-4ffe-b0f5-6e6f02cc0754"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
    "visible": true
}