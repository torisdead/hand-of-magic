{
    "id": "0fccd260-c8a9-4070-a296-b4d1a99b6c35",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hiddenwall_ceiling",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "b60ed426-fdf0-4db2-b77a-8d84b1d1d85a",
    "visible": true
}