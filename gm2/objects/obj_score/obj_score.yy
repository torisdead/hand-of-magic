{
    "id": "6dbaf18d-2950-4952-a717-da99e528c979",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_score",
    "eventList": [
        {
            "id": "bd16bd12-b891-4716-a3bb-dd60c60c2da1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 5,
            "m_owner": "6dbaf18d-2950-4952-a717-da99e528c979"
        },
        {
            "id": "afcc27e9-7ddd-4389-9d23-2571f30a0f30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6dbaf18d-2950-4952-a717-da99e528c979"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}