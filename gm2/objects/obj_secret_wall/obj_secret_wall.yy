{
    "id": "f3d8992b-2393-47ac-97d1-5468425585ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_secret_wall",
    "eventList": [
        {
            "id": "070eeecd-baad-43bb-be3d-a75e190d7bb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b199e8c3-5a3b-4d22-a4be-2e8fc98941b5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f3d8992b-2393-47ac-97d1-5468425585ff"
        },
        {
            "id": "2f94f108-d9b0-46aa-97e6-a0e209e0e229",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "adf7f896-2523-458f-ad32-088edf6eeeab",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f3d8992b-2393-47ac-97d1-5468425585ff"
        },
        {
            "id": "ebcf84b9-d2b3-450c-b783-f01c2e9c8b4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c734f965-837b-42fe-84a2-272856a93c20",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f3d8992b-2393-47ac-97d1-5468425585ff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a873a2b9-61ab-4759-9d6b-9ca8bbee9aac",
    "visible": true
}