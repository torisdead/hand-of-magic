{
    "id": "4461a57a-3e56-4a32-b5e7-cbf5a192c349",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_startgame",
    "eventList": [
        {
            "id": "bb16cf12-e4f3-456c-bd1a-537991b57f48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "4461a57a-3e56-4a32-b5e7-cbf5a192c349"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "93fc608c-0dc5-4732-a4ba-829bac640d3c",
    "visible": true
}