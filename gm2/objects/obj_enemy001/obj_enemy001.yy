{
    "id": "929e1008-0682-4088-ad35-f265eed47991",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy001",
    "eventList": [
        {
            "id": "961763a9-c553-49ba-a2d9-6e536e16e845",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "929e1008-0682-4088-ad35-f265eed47991"
        },
        {
            "id": "db6896cc-1cc3-46d0-bf80-c22376850ed0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "929e1008-0682-4088-ad35-f265eed47991"
        },
        {
            "id": "42bd6dfc-d7b6-4f47-9bf7-889ee088e849",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "929e1008-0682-4088-ad35-f265eed47991"
        },
        {
            "id": "04dbd12a-94ab-42d1-a653-3a59aa87f83d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "929e1008-0682-4088-ad35-f265eed47991"
        },
        {
            "id": "3c7c6bfc-edaa-46fe-9d63-1d3b1b69e4d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "929e1008-0682-4088-ad35-f265eed47991"
        },
        {
            "id": "04a7aee6-cf2e-4dd9-9a64-21f47da02030",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "db63e074-e648-4dd7-b166-10439aa3ea99",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "929e1008-0682-4088-ad35-f265eed47991"
        },
        {
            "id": "dd32242d-1819-4364-9a3d-7d395dd04100",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "adf7f896-2523-458f-ad32-088edf6eeeab",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "929e1008-0682-4088-ad35-f265eed47991"
        },
        {
            "id": "ef0a3dfd-694b-4fce-8240-f6a90385d28c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c734f965-837b-42fe-84a2-272856a93c20",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "929e1008-0682-4088-ad35-f265eed47991"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "a3d7bed6-c6b9-4e44-95c2-e0ae4f515aec",
    "visible": true
}