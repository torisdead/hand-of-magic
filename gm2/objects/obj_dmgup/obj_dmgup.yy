{
    "id": "fca1344a-fc0f-4201-9677-c393d01a2de5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dmgup",
    "eventList": [
        {
            "id": "92ad07c7-4c01-4217-9cbb-31ef0473035f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fca1344a-fc0f-4201-9677-c393d01a2de5"
        },
        {
            "id": "a880bb4a-b72f-46d2-a8da-b767c0b554be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "db63e074-e648-4dd7-b166-10439aa3ea99",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "fca1344a-fc0f-4201-9677-c393d01a2de5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
    "visible": true
}