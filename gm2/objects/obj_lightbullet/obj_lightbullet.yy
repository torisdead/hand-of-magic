{
    "id": "b199e8c3-5a3b-4d22-a4be-2e8fc98941b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lightbullet",
    "eventList": [
        {
            "id": "3ffc0aa1-5599-4187-9e9e-ad57a251ebb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b199e8c3-5a3b-4d22-a4be-2e8fc98941b5"
        },
        {
            "id": "30ecea47-3daf-473b-8c4f-36848e89e129",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b199e8c3-5a3b-4d22-a4be-2e8fc98941b5"
        },
        {
            "id": "02ea065b-3e91-4d9e-9ee6-45189ea576c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee8f4367-5f75-4d36-a505-3dc7488984ad",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b199e8c3-5a3b-4d22-a4be-2e8fc98941b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "b5c587ce-a25b-4c16-8e4e-5257f6360f95",
    "visible": true
}