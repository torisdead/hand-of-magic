{
    "id": "94b24d82-f3ff-46e9-ac96-a38acf03552d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_33",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "098786b9-843b-4d31-95f3-06fd78000345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94b24d82-f3ff-46e9-ac96-a38acf03552d",
            "compositeImage": {
                "id": "2d7dcab9-0693-494c-8263-484eba10fb0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "098786b9-843b-4d31-95f3-06fd78000345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c672fa1-36dc-4bfc-831b-fd4f15f1efec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "098786b9-843b-4d31-95f3-06fd78000345",
                    "LayerId": "b6b2ffdc-5ed6-4987-bc0a-b14fd1ef9712"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b6b2ffdc-5ed6-4987-bc0a-b14fd1ef9712",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94b24d82-f3ff-46e9-ac96-a38acf03552d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}