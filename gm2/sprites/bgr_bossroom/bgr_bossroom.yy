{
    "id": "68ce4083-62f7-4a80-8790-2fa14649aa19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgr_bossroom",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 2047,
    "bbox_left": 0,
    "bbox_right": 2047,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "50ef8b1f-1ad3-47e6-a247-06c818b1aab2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68ce4083-62f7-4a80-8790-2fa14649aa19",
            "compositeImage": {
                "id": "d77e5a43-34d8-4866-b39e-3467dc06ba9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50ef8b1f-1ad3-47e6-a247-06c818b1aab2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a92a27c9-d111-47b7-b873-4d75258e931f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50ef8b1f-1ad3-47e6-a247-06c818b1aab2",
                    "LayerId": "20531445-8bb7-44ad-bbc0-08a82fdfede1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2048,
    "layers": [
        {
            "id": "20531445-8bb7-44ad-bbc0-08a82fdfede1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68ce4083-62f7-4a80-8790-2fa14649aa19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 2048,
    "xorig": 0,
    "yorig": 0
}