{
    "id": "2577f4ac-be34-4585-b2d6-c4e6ea26a9ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgr_dungeonfloor002",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "ddd3228e-6759-4e86-af5c-13a404f66ac2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2577f4ac-be34-4585-b2d6-c4e6ea26a9ba",
            "compositeImage": {
                "id": "880284cf-0f29-43ea-b882-18f354a20b43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddd3228e-6759-4e86-af5c-13a404f66ac2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0c19870-406e-4102-8a34-d8dc2f4ce29e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddd3228e-6759-4e86-af5c-13a404f66ac2",
                    "LayerId": "87ab5c84-db99-4c8d-b7af-f0fe3036dcb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "87ab5c84-db99-4c8d-b7af-f0fe3036dcb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2577f4ac-be34-4585-b2d6-c4e6ea26a9ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}