{
    "id": "b5c587ce-a25b-4c16-8e4e-5257f6360f95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lightning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b7e9e5b8-90cb-4d05-902c-9cefd46f9dd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5c587ce-a25b-4c16-8e4e-5257f6360f95",
            "compositeImage": {
                "id": "0b45cbf0-be9c-479d-b4e0-d404914bd88a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7e9e5b8-90cb-4d05-902c-9cefd46f9dd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9fcae2c-01e4-40f3-82d9-94548815f03b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e9e5b8-90cb-4d05-902c-9cefd46f9dd0",
                    "LayerId": "7ff56bfe-b20b-4336-bf0f-9592d6e12046"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7ff56bfe-b20b-4336-bf0f-9592d6e12046",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5c587ce-a25b-4c16-8e4e-5257f6360f95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}