{
    "id": "a873a2b9-61ab-4759-9d6b-9ca8bbee9aac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_secret_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3ff58337-fd92-432a-b4a7-fcc56255c12f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a873a2b9-61ab-4759-9d6b-9ca8bbee9aac",
            "compositeImage": {
                "id": "e655b80f-dc98-486b-8509-dc78656e2a99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ff58337-fd92-432a-b4a7-fcc56255c12f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13de02f0-598d-420c-b70f-31ef098f4df2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ff58337-fd92-432a-b4a7-fcc56255c12f",
                    "LayerId": "deee0517-89ee-47d7-82ef-49bda7b231c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "deee0517-89ee-47d7-82ef-49bda7b231c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a873a2b9-61ab-4759-9d6b-9ca8bbee9aac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}