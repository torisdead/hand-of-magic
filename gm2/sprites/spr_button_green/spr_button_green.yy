{
    "id": "ecf81fec-f128-4fb5-83b8-d5368cbac408",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9feca922-ff3a-410a-abde-544f2adbbbca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf81fec-f128-4fb5-83b8-d5368cbac408",
            "compositeImage": {
                "id": "723dcc8f-abf1-48ca-819f-c1674a58ef80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9feca922-ff3a-410a-abde-544f2adbbbca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac7d885d-824d-422b-903c-201b40697b3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9feca922-ff3a-410a-abde-544f2adbbbca",
                    "LayerId": "908a325c-62fc-40bc-a4fb-e4e08124e061"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "908a325c-62fc-40bc-a4fb-e4e08124e061",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecf81fec-f128-4fb5-83b8-d5368cbac408",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}