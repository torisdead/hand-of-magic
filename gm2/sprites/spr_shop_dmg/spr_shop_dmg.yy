{
    "id": "ad1846ed-9974-48e1-b49d-6edb1fa440db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shop_dmg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2a4cc961-a146-462d-8765-938aa33025b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad1846ed-9974-48e1-b49d-6edb1fa440db",
            "compositeImage": {
                "id": "4454c61f-4587-4be8-a39b-f23aaa5e1785",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a4cc961-a146-462d-8765-938aa33025b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0da6423-a000-424e-a877-0344c92284ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a4cc961-a146-462d-8765-938aa33025b8",
                    "LayerId": "8cba34b5-7aab-4587-badc-2a7caf33414d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "8cba34b5-7aab-4587-badc-2a7caf33414d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad1846ed-9974-48e1-b49d-6edb1fa440db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}