{
    "id": "f0bb8c1e-1880-4568-903d-b47d84bbab83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6e175426-9ac5-4ae5-84b2-de5b1be2ad73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0bb8c1e-1880-4568-903d-b47d84bbab83",
            "compositeImage": {
                "id": "a370d658-e937-4633-92f9-8c10ffd96d51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e175426-9ac5-4ae5-84b2-de5b1be2ad73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfc8b6ec-d7bd-4abe-a6a7-0632825a0484",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e175426-9ac5-4ae5-84b2-de5b1be2ad73",
                    "LayerId": "adbe1e80-e00d-4c79-ba48-2a018e772f0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "adbe1e80-e00d-4c79-ba48-2a018e772f0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0bb8c1e-1880-4568-903d-b47d84bbab83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}