{
    "id": "228554b8-3b05-43f5-b476-1789501fcfa4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemybullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "22d69f45-4b71-4099-819d-0cec6d8fbca1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "228554b8-3b05-43f5-b476-1789501fcfa4",
            "compositeImage": {
                "id": "88c0fded-08a4-462b-a7cd-4b275a133aed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d69f45-4b71-4099-819d-0cec6d8fbca1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "144f25da-c0c0-4c75-ab4a-e6741f20c653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d69f45-4b71-4099-819d-0cec6d8fbca1",
                    "LayerId": "2b0e9d49-6841-491f-8329-da7d7fa83369"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2b0e9d49-6841-491f-8329-da7d7fa83369",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "228554b8-3b05-43f5-b476-1789501fcfa4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}