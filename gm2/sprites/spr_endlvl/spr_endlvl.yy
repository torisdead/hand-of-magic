{
    "id": "0d703379-723c-426b-abf8-bef184883e53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_endlvl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "813fd7aa-7a26-4b34-856b-212e859cb9a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d703379-723c-426b-abf8-bef184883e53",
            "compositeImage": {
                "id": "688afee3-f792-4b3b-9122-226f4166130f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "813fd7aa-7a26-4b34-856b-212e859cb9a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecbd8056-35c4-48c3-83d5-2db843894dd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "813fd7aa-7a26-4b34-856b-212e859cb9a5",
                    "LayerId": "0e17b4d9-96f5-448c-9a5e-65977d4ec92e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0e17b4d9-96f5-448c-9a5e-65977d4ec92e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d703379-723c-426b-abf8-bef184883e53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}