{
    "id": "9d9fcbde-b10f-4081-855b-b6550a2aec4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spdup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "08d72b4d-4136-4261-bc52-baf6f05d8bf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d9fcbde-b10f-4081-855b-b6550a2aec4f",
            "compositeImage": {
                "id": "a6bed331-476c-40e3-be07-14af19c8b10c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08d72b4d-4136-4261-bc52-baf6f05d8bf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6322db9e-382e-4b00-9aa9-a303ab496ab9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08d72b4d-4136-4261-bc52-baf6f05d8bf6",
                    "LayerId": "f7d11843-0cf6-49a2-bc3e-6f94fe5235d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f7d11843-0cf6-49a2-bc3e-6f94fe5235d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d9fcbde-b10f-4081-855b-b6550a2aec4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}