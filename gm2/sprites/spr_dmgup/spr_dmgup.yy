{
    "id": "e1179b1f-c923-450d-9275-2b59f9df7342",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dmgup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f2587aaa-c81c-4410-99c4-85c0138ed153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "caaefbae-eb06-452d-b8e6-3461b90f53ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2587aaa-c81c-4410-99c4-85c0138ed153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e67a259a-e06c-49ea-93f7-a1347aed9704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2587aaa-c81c-4410-99c4-85c0138ed153",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "7506b155-4d9c-4cc8-be6a-6ba0d554879d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "a8344fdc-2e90-44b5-b37a-09dffb84d384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7506b155-4d9c-4cc8-be6a-6ba0d554879d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15e334a0-4ad5-4c1d-b442-01aad84ecbd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7506b155-4d9c-4cc8-be6a-6ba0d554879d",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "eaff4cfd-92da-4b70-b5e7-f5a0cc2ae6ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "7ffdbeaa-80ee-4ab3-9bf3-50daf228f530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaff4cfd-92da-4b70-b5e7-f5a0cc2ae6ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4c7fb63-1813-49d1-8081-a84e633025f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaff4cfd-92da-4b70-b5e7-f5a0cc2ae6ff",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "edd58a21-9c6e-4038-bd9d-128d23592c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "e5f9b7dc-dc46-4166-ab0a-55de31cf568b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edd58a21-9c6e-4038-bd9d-128d23592c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02eafd2b-f96e-497f-8205-967bdf9ad8f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edd58a21-9c6e-4038-bd9d-128d23592c92",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "91b5e9dd-6b40-4582-bfac-b21675f8fd6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "c227ab54-9b0c-4f17-99d4-c713e202f631",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91b5e9dd-6b40-4582-bfac-b21675f8fd6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f11cc1f8-0be1-4f35-a34b-199d6896242c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91b5e9dd-6b40-4582-bfac-b21675f8fd6f",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "0fabef2e-6bb8-4226-a4a4-8eb671a09ddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "6a43dbe6-3968-4576-81fc-4d233d1b4fbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fabef2e-6bb8-4226-a4a4-8eb671a09ddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d73e761-566e-4e0a-a4c2-8783e4ed05b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fabef2e-6bb8-4226-a4a4-8eb671a09ddb",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "bf6cba96-4037-495c-8fab-eb78abdf5c45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "f1327c06-e1e4-4d4b-be0f-7dc1dc12bb06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf6cba96-4037-495c-8fab-eb78abdf5c45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b968a236-a9a8-423a-a878-96331ee07aa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf6cba96-4037-495c-8fab-eb78abdf5c45",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "3cf71313-f3de-4416-a5e2-753a632fdf5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "1ec06711-589c-43e8-ae7f-1a939919b573",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf71313-f3de-4416-a5e2-753a632fdf5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0e5e3d0-cbcd-46cf-8681-20651dd9ea3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf71313-f3de-4416-a5e2-753a632fdf5f",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "cc38de1c-a9fa-4733-bb67-3bb34f7bc6f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "bd264f37-5e19-4428-b592-25314d437060",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc38de1c-a9fa-4733-bb67-3bb34f7bc6f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "726df374-8a21-4a30-bd70-0ebc5817c0be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc38de1c-a9fa-4733-bb67-3bb34f7bc6f9",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "2ed5d66a-1a8b-46d9-bb0b-5818c901e5f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "fcd97ffa-d3a7-4a58-9b40-522476e1a396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ed5d66a-1a8b-46d9-bb0b-5818c901e5f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70d26cff-4ea6-438b-8f19-601a8cdde976",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ed5d66a-1a8b-46d9-bb0b-5818c901e5f7",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "54064404-349d-40eb-8b29-8a13482950f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "8a8eb385-4189-4ddf-a165-cd6a32bfe78f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54064404-349d-40eb-8b29-8a13482950f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab25a4ae-aa35-46ca-98d8-078570c0aafa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54064404-349d-40eb-8b29-8a13482950f4",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "f67c03e4-3358-4249-9142-18e2b6cee50f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "eef25972-392a-4413-9a07-1127c4529fc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f67c03e4-3358-4249-9142-18e2b6cee50f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dc12ea7-7f29-4e8d-ba03-9f7a4f16b46c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f67c03e4-3358-4249-9142-18e2b6cee50f",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "de139be0-dd2f-45bd-a5b9-e7884adae44c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "b9ed186a-182d-40e1-bc74-f147c11cc922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de139be0-dd2f-45bd-a5b9-e7884adae44c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "071eb9aa-88e1-4359-beb6-10e286649456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de139be0-dd2f-45bd-a5b9-e7884adae44c",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "4007b89e-3720-4207-812f-662cd4284553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "1d42ee8e-b51f-49dd-bbd7-289bd61efc04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4007b89e-3720-4207-812f-662cd4284553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48d410b5-746a-40ef-b55e-bbba94cc1938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4007b89e-3720-4207-812f-662cd4284553",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        },
        {
            "id": "f58a0872-6719-4512-8cc4-83b5562d66d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "compositeImage": {
                "id": "1678972f-4617-49a0-91d0-003c4285e90b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f58a0872-6719-4512-8cc4-83b5562d66d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f119b06f-1428-498f-8229-7f0d155138f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f58a0872-6719-4512-8cc4-83b5562d66d9",
                    "LayerId": "423d8845-9bd8-404a-90b4-173efbf96f5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "423d8845-9bd8-404a-90b4-173efbf96f5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1179b1f-c923-450d-9275-2b59f9df7342",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}