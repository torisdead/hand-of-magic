{
    "id": "ef44c473-82f9-4f01-846d-a57c90ba15a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgr_swamp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "84e28d7e-5c19-4161-abeb-5abdad375102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef44c473-82f9-4f01-846d-a57c90ba15a6",
            "compositeImage": {
                "id": "049e4cdc-2e02-44d5-813c-1b6264b14878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84e28d7e-5c19-4161-abeb-5abdad375102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "838b8175-292c-488a-b414-ad8406123ade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84e28d7e-5c19-4161-abeb-5abdad375102",
                    "LayerId": "54c19ce5-ba61-4c20-af03-bf6270a6a495"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "54c19ce5-ba61-4c20-af03-bf6270a6a495",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef44c473-82f9-4f01-846d-a57c90ba15a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}