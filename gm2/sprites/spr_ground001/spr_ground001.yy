{
    "id": "1fb82ef0-085c-4c1b-aa5e-459b81579b6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground001",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c45b16c2-c58e-4259-a1d6-31eeeaf24080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fb82ef0-085c-4c1b-aa5e-459b81579b6c",
            "compositeImage": {
                "id": "b9238b0f-706b-41b4-87d3-bcf11ec34658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c45b16c2-c58e-4259-a1d6-31eeeaf24080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbcb087e-8e90-490c-a22b-145186f2bb2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c45b16c2-c58e-4259-a1d6-31eeeaf24080",
                    "LayerId": "6d9a6c7a-c90c-44ad-ba55-4a04c1e94b55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6d9a6c7a-c90c-44ad-ba55-4a04c1e94b55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fb82ef0-085c-4c1b-aa5e-459b81579b6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}