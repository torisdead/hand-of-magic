{
    "id": "92be3409-0a25-4460-a9ac-b1ba4c9f3f10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_save3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9444db70-cbd1-43e7-af95-3ef2fea5142d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92be3409-0a25-4460-a9ac-b1ba4c9f3f10",
            "compositeImage": {
                "id": "a6df42e9-1221-4715-9ca4-918e278c5ddf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9444db70-cbd1-43e7-af95-3ef2fea5142d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78fa101d-697c-416c-a6c4-f8dfca7e8c8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9444db70-cbd1-43e7-af95-3ef2fea5142d",
                    "LayerId": "74358556-76af-4e84-84d6-93103fae14cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "74358556-76af-4e84-84d6-93103fae14cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92be3409-0a25-4460-a9ac-b1ba4c9f3f10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}