{
    "id": "f3de8024-f507-476e-a0c1-7e8b28dbdc16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hpupgrade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "275b79d2-7887-4368-84e9-089af9943570",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3de8024-f507-476e-a0c1-7e8b28dbdc16",
            "compositeImage": {
                "id": "7741ef93-a294-4359-afed-323e1cedf6dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "275b79d2-7887-4368-84e9-089af9943570",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f5de54-66cf-4111-a934-334d134f0aea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "275b79d2-7887-4368-84e9-089af9943570",
                    "LayerId": "dfcfd220-c296-4c6b-98a1-18bcaf88bdc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dfcfd220-c296-4c6b-98a1-18bcaf88bdc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3de8024-f507-476e-a0c1-7e8b28dbdc16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}