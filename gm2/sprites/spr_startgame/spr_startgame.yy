{
    "id": "93fc608c-0dc5-4732-a4ba-829bac640d3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_startgame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 359,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "201d5af6-529c-4681-a3eb-6e51660e80c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93fc608c-0dc5-4732-a4ba-829bac640d3c",
            "compositeImage": {
                "id": "21dae7af-159f-4f69-8930-44906232361c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "201d5af6-529c-4681-a3eb-6e51660e80c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8331b5cd-1f01-43dc-8954-a9bdeb1840e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "201d5af6-529c-4681-a3eb-6e51660e80c8",
                    "LayerId": "176841a7-7b72-463d-9a4c-430979fc0dba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "176841a7-7b72-463d-9a4c-430979fc0dba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93fc608c-0dc5-4732-a4ba-829bac640d3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 360,
    "xorig": 0,
    "yorig": 0
}