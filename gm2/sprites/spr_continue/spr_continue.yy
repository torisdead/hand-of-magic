{
    "id": "882e15ac-4cef-47cf-9440-a714cb781a94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_continue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "77734118-2503-489f-b253-3938d8aa206a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "882e15ac-4cef-47cf-9440-a714cb781a94",
            "compositeImage": {
                "id": "2e3d53bb-b2cd-4e31-b2f8-41b16fed379d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77734118-2503-489f-b253-3938d8aa206a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a72634e1-00b8-4fda-8d80-c7f5cfbc311e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77734118-2503-489f-b253-3938d8aa206a",
                    "LayerId": "e1939deb-d27b-4749-8537-e8d906d1a2fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "e1939deb-d27b-4749-8537-e8d906d1a2fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "882e15ac-4cef-47cf-9440-a714cb781a94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}