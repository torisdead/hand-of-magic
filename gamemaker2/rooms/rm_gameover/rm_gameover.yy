{
    "id": "f7f41ad4-20fe-47a7-93bf-cf4e19251989",
    "modelName": "GMRoom",
    "mvc": "1.0",
    "name": "rm_gameover",
    "IsDnD": false,
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "35c6bdf2-08f1-4119-8579-b8f0eba19c5c",
        "05c463f1-f710-4785-9506-2b6366b0eab1"
    ],
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "id": "befe48cc-d421-4374-a12d-cf7b14fb600b",
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [
                
            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "name": "Compatibility_Instances_Depth_0",
            "userdefined_depth": true,
            "visible": true,
            "instances": [
                {
                    "id": "35c6bdf2-08f1-4119-8579-b8f0eba19c5c",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": false,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_1009F299",
                    "x": 512,
                    "y": 352,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_1009F299",
                    "objId": "7961ba5b-d98b-4c5b-be72-27f5712270bc",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                },
                {
                    "id": "05c463f1-f710-4785-9506-2b6366b0eab1",
                    "modelName": "GMRInstance",
                    "mvc": "1.0",
                    "ignore": false,
                    "inheritItemSettings": false,
                    "m_originalParentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "name": "inst_DA6098BE",
                    "x": 352,
                    "y": 480,
                    "IsDnD": false,
                    "colour": {
                        "Value": 4294967295
                    },
                    "creationCodeFile": "",
                    "creationCodeType": "",
                    "inheritCode": false,
                    "name_with_no_file_rename": "inst_DA6098BE",
                    "objId": "a67c4ec8-fba2-458e-ab5b-5fa72e1aabe0",
                    "rotation": 0,
                    "scaleX": 1,
                    "scaleY": 1
                }
            ]
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "id": "63aabd66-5884-426a-9f2b-73f92ec49eff",
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "depth": 2147483500,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [
                
            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "name": "Compatibility_Background_0_bgr_fire",
            "userdefined_depth": true,
            "visible": true,
            "animationFPS": 15,
            "animationSpeedType": 0,
            "colour": {
                "Value": 4294967295
            },
            "hspeed": 1,
            "htiled": true,
            "spriteId": "d5fec398-a7bb-4a4e-a866-fe3cd918599a",
            "stretch": false,
            "userdefined_animFPS": false,
            "vspeed": -1,
            "vtiled": true,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "id": "a4994626-39c1-403f-b833-61314eb1a175",
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "depth": 2147483600,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [
                
            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "name": "Compatibility_Colour",
            "userdefined_depth": true,
            "visible": true,
            "animationFPS": 15,
            "animationSpeedType": 0,
            "colour": {
                "Value": 4290822336
            },
            "hspeed": 0,
            "htiled": false,
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings": {
        "id": "a0b0be30-424e-4c82-83f6-f06e6b75f962",
        "modelName": "GMRoomPhysicsSettings",
        "mvc": "1.0",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "inheritPhysicsSettings": false
    },
    "roomSettings": {
        "id": "7e2ba6a1-5bd6-49be-858d-ef15b4f3751d",
        "modelName": "GMRoomSettings",
        "mvc": "1.0",
        "Height": 768,
        "Width": 1024,
        "inheritRoomSettings": false,
        "persistent": false
    },
    "viewSettings": {
        "id": "fa6e31c7-9c0c-41e1-9513-28f80f04e0ff",
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0",
        "clearDisplayBuffer": true,
        "clearViewBackground": true,
        "enableViews": false,
        "inheritViewSettings": false
    },
    "views": [
        {
            "id": "7c0ba958-bed2-4e60-88bd-882f729b1d84",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "27544841-b6ae-417d-a7ce-cc3fe3aba664",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "bfaf997c-a4ce-4c9f-ab0f-32b9a1448ca3",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "7ed70cdd-4e40-4c49-b47f-f311a747cc91",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "0637cf57-520a-47e1-b32b-93d1f704ec80",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "0d965d1d-1176-4394-9fb8-c9f555c70665",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "6a9c54e9-ea32-49b1-b16b-36be48e2aa9f",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        },
        {
            "id": "eed55209-bf54-4b13-850c-0d9a297403b0",
            "modelName": "GMRView",
            "mvc": "1.0",
            "hborder": 32,
            "hport": 768,
            "hspeed": -1,
            "hview": 768,
            "inherit": false,
            "objId": "00000000-0000-0000-0000-000000000000",
            "vborder": 32,
            "visible": false,
            "vspeed": -1,
            "wport": 1024,
            "wview": 1024,
            "xport": 0,
            "xview": 0,
            "yport": 0,
            "yview": 0
        }
    ]
}