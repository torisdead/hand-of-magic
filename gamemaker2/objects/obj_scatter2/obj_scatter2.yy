{
    "id": "c734f965-837b-42fe-84a2-272856a93c20",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scatter2",
    "eventList": [
        {
            "id": "793e8604-ff59-4d38-b9fc-e33e86c34943",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c734f965-837b-42fe-84a2-272856a93c20"
        },
        {
            "id": "8de4fd07-2974-41e2-a7db-c9462d76f4bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee8f4367-5f75-4d36-a505-3dc7488984ad",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c734f965-837b-42fe-84a2-272856a93c20"
        },
        {
            "id": "06203b05-d13a-4240-b481-2091393d5839",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "929e1008-0682-4088-ad35-f265eed47991",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c734f965-837b-42fe-84a2-272856a93c20"
        },
        {
            "id": "de238d2c-5449-46a8-8667-b030c629611d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1bfc9e6d-5d3d-434d-9163-1a750cfd0689",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c734f965-837b-42fe-84a2-272856a93c20"
        },
        {
            "id": "ff9b9cb9-b958-4de1-ba49-470a357b18e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c734f965-837b-42fe-84a2-272856a93c20"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "b5c587ce-a25b-4c16-8e4e-5257f6360f95",
    "visible": true
}