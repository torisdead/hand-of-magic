{
    "id": "ee8f4367-5f75-4d36-a505-3dc7488984ad",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall001",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "76f1b183-4cc5-4e57-8cdc-23962eda0c8b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": -16,
            "y": -16
        },
        {
            "id": "4d2c85c5-f843-4b38-b1b6-2e25ee998048",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": -16
        },
        {
            "id": "9bb29521-ff77-40c0-ae81-6a1c820e0353",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "bf2a7099-5af6-43dd-ac50-cb60d3fc58b7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": -16,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "solid": true,
    "spriteId": "12d7672f-47ee-43c9-aba2-671d5c4931d4",
    "visible": true
}