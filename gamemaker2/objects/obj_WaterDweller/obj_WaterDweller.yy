{
    "id": "80056460-dc36-4936-ab6c-4bdd5aebd476",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_WaterDweller",
    "eventList": [
        {
            "id": "9dd08e9d-4761-4229-ac90-bcf4da51d386",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "80056460-dc36-4936-ab6c-4bdd5aebd476"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "002bf388-5be3-4ffb-b066-11fde2fbe3e1",
    "visible": true
}