{
    "id": "2bcba37c-3c54-46c4-9393-3c4675324dde",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shop",
    "eventList": [
        {
            "id": "775e64a3-fb74-4ebf-a0ff-7d0a95a36ab1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "db63e074-e648-4dd7-b166-10439aa3ea99",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2bcba37c-3c54-46c4-9393-3c4675324dde"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "1e77fc13-f99e-47a6-afe7-9e2fae2948fb",
    "visible": true
}