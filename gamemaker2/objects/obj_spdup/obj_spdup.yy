{
    "id": "b875f98e-6e8b-4b05-be08-054cf272839e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spdup",
    "eventList": [
        {
            "id": "10a39e8b-f2df-4d22-b26b-362adf4093c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "db63e074-e648-4dd7-b166-10439aa3ea99",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b875f98e-6e8b-4b05-be08-054cf272839e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "9d9fcbde-b10f-4081-855b-b6550a2aec4f",
    "visible": true
}