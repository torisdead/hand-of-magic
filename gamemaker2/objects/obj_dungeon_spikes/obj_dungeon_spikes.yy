{
    "id": "55e37427-6d0c-4161-bf89-e21df3a3a145",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dungeon_spikes",
    "eventList": [
        {
            "id": "2eec187f-5726-4c0b-9cc3-9f1bc2310e07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "db63e074-e648-4dd7-b166-10439aa3ea99",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "55e37427-6d0c-4161-bf89-e21df3a3a145"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "c5500a78-06e9-4daf-9ec6-03c57ac25134",
    "visible": true
}