{
    "id": "a67c4ec8-fba2-458e-ab5b-5fa72e1aabe0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_playagain",
    "eventList": [
        {
            "id": "7c176865-8888-4339-ae19-a07c49f08fb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "a67c4ec8-fba2-458e-ab5b-5fa72e1aabe0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "93fc608c-0dc5-4732-a4ba-829bac640d3c",
    "visible": true
}