{
    "id": "46f013db-14f5-489a-a0ca-f18228718191",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemybullet",
    "eventList": [
        {
            "id": "16f038c7-f30f-488b-bcea-e68167082a40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "46f013db-14f5-489a-a0ca-f18228718191"
        },
        {
            "id": "88822e5a-7bc7-46f2-b0b3-15bda5a3d3b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "46f013db-14f5-489a-a0ca-f18228718191"
        },
        {
            "id": "acd136d4-2c37-492c-82e2-61dff54c94c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee8f4367-5f75-4d36-a505-3dc7488984ad",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "46f013db-14f5-489a-a0ca-f18228718191"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "228554b8-3b05-43f5-b476-1789501fcfa4",
    "visible": true
}