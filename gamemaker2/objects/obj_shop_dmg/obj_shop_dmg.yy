{
    "id": "a096507c-c065-421c-b236-c46074334d77",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shop_dmg",
    "eventList": [
        {
            "id": "899743fe-9dde-417a-a9cb-a32506d453cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "a096507c-c065-421c-b236-c46074334d77"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "ad1846ed-9974-48e1-b49d-6edb1fa440db",
    "visible": true
}