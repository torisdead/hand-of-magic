{
    "id": "c6d6db72-14e7-4f9e-9813-f8aca5f56757",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shop_hp",
    "eventList": [
        {
            "id": "bc621dfa-1a6a-4ed8-8a97-f6fef30679b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "c6d6db72-14e7-4f9e-9813-f8aca5f56757"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "d7d6b942-1095-478a-a8f6-8b15df95414a",
    "visible": true
}