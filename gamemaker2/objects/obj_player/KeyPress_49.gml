/// scatter shot
// You can write your code in this editor
if global.scatter >= 1
{
global.scatter -= 1;
instance_create(x,y,obj_lightbullet);
instance_create(x,y,obj_scatter1);
instance_create(x,y,obj_scatter2);
}
if global.scatter = 0
audio_play_sound(snd_oncd, 10, false);