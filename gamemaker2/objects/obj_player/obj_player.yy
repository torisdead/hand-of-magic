{
    "id": "db63e074-e648-4dd7-b166-10439aa3ea99",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "feef84b0-c820-469b-86e7-7c4ba1854081",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        },
        {
            "id": "59df1e69-0cf5-4460-b10a-c3c2193e202f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        },
        {
            "id": "8b1425d4-2d52-4a99-b4ac-c7113d7506da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        },
        {
            "id": "61fa1d8d-a941-4eba-be93-df18663f1092",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        },
        {
            "id": "32040477-b667-4eef-9d57-e2b22bee427a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2a657f94-3509-4c42-9ed6-4766e8277510",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        },
        {
            "id": "cdcfc0ff-b69e-430b-af63-523d882c3168",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        },
        {
            "id": "b2886613-77b7-4883-9290-ce299a5aabbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        },
        {
            "id": "d70cb767-da81-40c0-9fa7-a8c46fb09c1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 9,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        },
        {
            "id": "8bd1e4da-75ec-4497-b003-dfd83eb659d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 49,
            "eventtype": 9,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        },
        {
            "id": "d97b4eb5-b297-4526-a624-4fe0482bcfcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        },
        {
            "id": "e074bfd8-cac3-4028-b6e1-d361403b12f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        },
        {
            "id": "675c04bc-9a29-4d2d-8f24-bf52137ed3e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "db63e074-e648-4dd7-b166-10439aa3ea99"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "6c9aa5f9-40fd-4031-8f85-0831a58ef232",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "69e1c7bf-959f-4d69-aa2e-fe5a79a90ac0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "be1798e8-21f1-49c3-ae28-58ddafd798f5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "cf81516f-4b11-4874-aa1b-8e719340628d",
    "visible": true
}