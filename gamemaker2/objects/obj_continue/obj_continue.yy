{
    "id": "1ddf1656-a39a-480d-808e-ce7bdddfd0a3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_continue",
    "eventList": [
        {
            "id": "3b51450f-a532-4dd6-8d92-236ae0067c0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "1ddf1656-a39a-480d-808e-ce7bdddfd0a3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "882e15ac-4cef-47cf-9440-a714cb781a94",
    "visible": true
}