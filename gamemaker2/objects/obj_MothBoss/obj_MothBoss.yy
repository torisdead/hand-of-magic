{
    "id": "1bfc9e6d-5d3d-434d-9163-1a750cfd0689",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_MothBoss",
    "eventList": [
        {
            "id": "dacdce27-3c04-4152-b5ed-15c145cf47d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1bfc9e6d-5d3d-434d-9163-1a750cfd0689"
        },
        {
            "id": "685fd560-dc05-43eb-839b-e0e77d30d4aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1bfc9e6d-5d3d-434d-9163-1a750cfd0689"
        },
        {
            "id": "9e9a5ed0-b610-4fce-a886-226f47bc7786",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1bfc9e6d-5d3d-434d-9163-1a750cfd0689"
        },
        {
            "id": "486f3194-690a-46d6-b206-a02c942c2358",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1bfc9e6d-5d3d-434d-9163-1a750cfd0689"
        },
        {
            "id": "7aab7db7-6063-4175-8fce-62180ea1c77c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b199e8c3-5a3b-4d22-a4be-2e8fc98941b5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1bfc9e6d-5d3d-434d-9163-1a750cfd0689"
        },
        {
            "id": "46c5ef1d-39ef-45c4-9615-2f60d9156a8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "db63e074-e648-4dd7-b166-10439aa3ea99",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1bfc9e6d-5d3d-434d-9163-1a750cfd0689"
        },
        {
            "id": "f47fca8e-8fb3-440b-a7bf-69c8081a3bf0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "adf7f896-2523-458f-ad32-088edf6eeeab",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1bfc9e6d-5d3d-434d-9163-1a750cfd0689"
        },
        {
            "id": "98edd08f-f1fb-42e9-a5c5-31ac7c37e090",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c734f965-837b-42fe-84a2-272856a93c20",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1bfc9e6d-5d3d-434d-9163-1a750cfd0689"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8d16667f-3b90-41db-a335-67fb6d99fea6",
    "visible": true
}