{
    "id": "adf7f896-2523-458f-ad32-088edf6eeeab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scatter1",
    "eventList": [
        {
            "id": "ed94bda1-6ffb-4f81-8759-037310def11e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "adf7f896-2523-458f-ad32-088edf6eeeab"
        },
        {
            "id": "b2ab378b-cf6d-4ed3-a2b0-1f5d907510c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "929e1008-0682-4088-ad35-f265eed47991",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "adf7f896-2523-458f-ad32-088edf6eeeab"
        },
        {
            "id": "c2458bc7-4842-4f31-af94-cdfa3db07d7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee8f4367-5f75-4d36-a505-3dc7488984ad",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "adf7f896-2523-458f-ad32-088edf6eeeab"
        },
        {
            "id": "ec1e314f-002d-4bd1-bec4-b67469495c5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1bfc9e6d-5d3d-434d-9163-1a750cfd0689",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "adf7f896-2523-458f-ad32-088edf6eeeab"
        },
        {
            "id": "cf25ea57-b778-442e-b1ef-6827e8828f57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "adf7f896-2523-458f-ad32-088edf6eeeab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "b5c587ce-a25b-4c16-8e4e-5257f6360f95",
    "visible": true
}