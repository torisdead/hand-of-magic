{
    "id": "0fccd260-c8a9-4070-a296-b4d1a99b6c35",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hiddenwall_ceiling",
    "eventList": [
        {
            "id": "9c5ab8a2-d068-490e-aff1-5a6e420ece52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "db63e074-e648-4dd7-b166-10439aa3ea99",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0fccd260-c8a9-4070-a296-b4d1a99b6c35"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "12d7672f-47ee-43c9-aba2-671d5c4931d4",
    "visible": true
}