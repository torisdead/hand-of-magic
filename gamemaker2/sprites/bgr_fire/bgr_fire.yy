{
    "id": "d5fec398-a7bb-4a4e-a866-fe3cd918599a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgr_fire",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 899,
    "bbox_left": 0,
    "bbox_right": 899,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "cb17b38c-7186-47ab-a0e4-30fcfed30842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5fec398-a7bb-4a4e-a866-fe3cd918599a",
            "compositeImage": {
                "id": "fef4dff0-839f-445f-b68f-d193ad6c22d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb17b38c-7186-47ab-a0e4-30fcfed30842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3281f58-9495-4ac6-b047-3ed6b6ee2896",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb17b38c-7186-47ab-a0e4-30fcfed30842",
                    "LayerId": "dcf84f53-7f8d-4ebc-b9ce-7325c2a8205c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 900,
    "layers": [
        {
            "id": "dcf84f53-7f8d-4ebc-b9ce-7325c2a8205c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5fec398-a7bb-4a4e-a866-fe3cd918599a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}