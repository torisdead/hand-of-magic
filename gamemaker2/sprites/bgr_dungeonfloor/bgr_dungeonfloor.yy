{
    "id": "44bbe219-0f7d-44f0-a876-b76ee77d5cf6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgr_dungeonfloor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c65787b5-1ef6-4b5f-8e07-b88186802592",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44bbe219-0f7d-44f0-a876-b76ee77d5cf6",
            "compositeImage": {
                "id": "65d9aa2c-d44f-4420-9c65-7003691a8b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c65787b5-1ef6-4b5f-8e07-b88186802592",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90b52aad-dc51-41b0-812c-f215aa73e751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c65787b5-1ef6-4b5f-8e07-b88186802592",
                    "LayerId": "e202d161-ecab-4017-a5fe-8e85fe833170"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e202d161-ecab-4017-a5fe-8e85fe833170",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44bbe219-0f7d-44f0-a876-b76ee77d5cf6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}