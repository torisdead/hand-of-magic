{
    "id": "15c5abea-093f-4402-b61f-df83f292506e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_38",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4a71815d-898d-4f80-a375-c74b5d7cea69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15c5abea-093f-4402-b61f-df83f292506e",
            "compositeImage": {
                "id": "1cbc2d5b-5189-4a65-9de8-a7fffcbcacad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a71815d-898d-4f80-a375-c74b5d7cea69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9a7be9a-0ae0-41e5-aefb-e0df12605ded",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a71815d-898d-4f80-a375-c74b5d7cea69",
                    "LayerId": "85ae2dc2-582f-4a64-a295-141ff645a699"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "85ae2dc2-582f-4a64-a295-141ff645a699",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15c5abea-093f-4402-b61f-df83f292506e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}