{
    "id": "b1061a4f-be9a-4e95-9796-7568c20eb27c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background0",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7d476dfb-f9d5-45f1-940e-7c13bde1ebe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1061a4f-be9a-4e95-9796-7568c20eb27c",
            "compositeImage": {
                "id": "641c4b35-438b-4326-926d-de5fac4e0b8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d476dfb-f9d5-45f1-940e-7c13bde1ebe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d0e2fb4-63fb-430a-ac6e-7d9f0ec0efd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d476dfb-f9d5-45f1-940e-7c13bde1ebe9",
                    "LayerId": "2634ed94-21e6-4afc-861d-18adba956444"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "2634ed94-21e6-4afc-861d-18adba956444",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1061a4f-be9a-4e95-9796-7568c20eb27c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 200,
    "xorig": 0,
    "yorig": 0
}