{
    "id": "a5da1cc6-b6a7-4162-83bf-78d63645e898",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GreenBeast",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e26125e4-586b-4666-a6cd-3fe03e84fde4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5da1cc6-b6a7-4162-83bf-78d63645e898",
            "compositeImage": {
                "id": "4c46bd82-e413-4b19-ac62-684f0f354f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e26125e4-586b-4666-a6cd-3fe03e84fde4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4913002-dfb7-4de6-9828-8e6bb1e41ae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e26125e4-586b-4666-a6cd-3fe03e84fde4",
                    "LayerId": "42e844f4-463d-491f-914a-42cca627b242"
                }
            ]
        },
        {
            "id": "63ccd2f4-9225-483e-be4c-8acc2feac1c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5da1cc6-b6a7-4162-83bf-78d63645e898",
            "compositeImage": {
                "id": "6b0785d8-3e75-4b46-b618-c062b0a416cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63ccd2f4-9225-483e-be4c-8acc2feac1c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bfadf28-aa50-4827-87cb-3cae0b9a2737",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63ccd2f4-9225-483e-be4c-8acc2feac1c9",
                    "LayerId": "42e844f4-463d-491f-914a-42cca627b242"
                }
            ]
        },
        {
            "id": "6a43547b-0d6a-47ec-9359-2614e918a120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5da1cc6-b6a7-4162-83bf-78d63645e898",
            "compositeImage": {
                "id": "f6f682f7-f875-456c-9bda-c70e28fff535",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a43547b-0d6a-47ec-9359-2614e918a120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a1009e1-e108-4ef4-99b3-ec7b4c383c46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a43547b-0d6a-47ec-9359-2614e918a120",
                    "LayerId": "42e844f4-463d-491f-914a-42cca627b242"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "42e844f4-463d-491f-914a-42cca627b242",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5da1cc6-b6a7-4162-83bf-78d63645e898",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}