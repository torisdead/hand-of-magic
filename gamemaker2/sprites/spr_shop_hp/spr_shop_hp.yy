{
    "id": "d7d6b942-1095-478a-a8f6-8b15df95414a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shop_hp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f9ec8b92-3daa-4a93-8666-407b3a2106c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7d6b942-1095-478a-a8f6-8b15df95414a",
            "compositeImage": {
                "id": "475b17aa-b6b2-4af3-a98c-3efe99230474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9ec8b92-3daa-4a93-8666-407b3a2106c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d47bbcb5-41e0-4db1-b5ad-c86a7c5938cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9ec8b92-3daa-4a93-8666-407b3a2106c6",
                    "LayerId": "6dcf795c-8425-403a-8f95-aa6a8544c96f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "6dcf795c-8425-403a-8f95-aa6a8544c96f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7d6b942-1095-478a-a8f6-8b15df95414a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}