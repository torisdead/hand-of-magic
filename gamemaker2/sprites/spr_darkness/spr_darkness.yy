{
    "id": "f2bf2c37-fe55-49d4-b266-e2ece494b2eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_darkness",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "23860f2f-da06-4a2f-94d1-99afc4ca24d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2bf2c37-fe55-49d4-b266-e2ece494b2eb",
            "compositeImage": {
                "id": "6f78c183-8eb8-4e79-8ef7-376811dbcefb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23860f2f-da06-4a2f-94d1-99afc4ca24d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "044fede0-33f0-419a-93b4-4b738f694de1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23860f2f-da06-4a2f-94d1-99afc4ca24d6",
                    "LayerId": "c9878e48-26f9-4e4f-aadd-e9cc91a100bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c9878e48-26f9-4e4f-aadd-e9cc91a100bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2bf2c37-fe55-49d4-b266-e2ece494b2eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}