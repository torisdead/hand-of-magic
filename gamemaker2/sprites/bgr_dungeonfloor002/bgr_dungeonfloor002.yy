{
    "id": "2577f4ac-be34-4585-b2d6-c4e6ea26a9ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgr_dungeonfloor002",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d7a48018-7515-4469-99d4-8e0a67b040e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2577f4ac-be34-4585-b2d6-c4e6ea26a9ba",
            "compositeImage": {
                "id": "4fa500dc-935c-4f5c-9bf6-c865231b85d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7a48018-7515-4469-99d4-8e0a67b040e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99a96d79-6458-43b7-89c9-96d282352619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7a48018-7515-4469-99d4-8e0a67b040e7",
                    "LayerId": "a15189ae-266e-40f2-9da4-5946bc2c31eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "a15189ae-266e-40f2-9da4-5946bc2c31eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2577f4ac-be34-4585-b2d6-c4e6ea26a9ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}