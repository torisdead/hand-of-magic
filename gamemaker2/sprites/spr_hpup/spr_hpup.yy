{
    "id": "a8dc238e-25c6-4650-82c4-804c09068287",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hpup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f1f0eab6-c279-4b9e-a584-e27630f7e91a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "f148f22a-bc38-461e-a0f6-70ab9ac5af1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f0eab6-c279-4b9e-a584-e27630f7e91a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e3d1e2-9b73-43ea-893b-c04c7b8344dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f0eab6-c279-4b9e-a584-e27630f7e91a",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "5101e023-d402-4d96-bea8-04df73d93485",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "57fb16d1-5cb4-4a17-8d5f-c83224961ff9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5101e023-d402-4d96-bea8-04df73d93485",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3a36aea-56cf-44af-aac4-5c1bab30856d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5101e023-d402-4d96-bea8-04df73d93485",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "37d342b2-a381-46fe-877a-2ac7c8ad8fc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "80afe312-3256-43e4-b647-ba23b4457750",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37d342b2-a381-46fe-877a-2ac7c8ad8fc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8556aad-4a38-47b8-ab56-b957c3a2f2d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37d342b2-a381-46fe-877a-2ac7c8ad8fc4",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "f9d6650a-5c38-4c29-a3b2-6b2dc303e250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "45e479ae-1bdb-43b4-888d-24ec5b88e8bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9d6650a-5c38-4c29-a3b2-6b2dc303e250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ea8b752-b92d-4fdd-8e43-6262ebceb579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9d6650a-5c38-4c29-a3b2-6b2dc303e250",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "31be66b7-6a5c-4279-9e1c-513886d6b0d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "43835e78-ecd9-42b3-8810-8ded0ebc0549",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31be66b7-6a5c-4279-9e1c-513886d6b0d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0489446d-5820-4703-8b4c-671d6280d86b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31be66b7-6a5c-4279-9e1c-513886d6b0d7",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "510ff818-4456-4e6d-898b-e285baaf18c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "75cd449b-15af-4d8a-afec-afe2fb37e05e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "510ff818-4456-4e6d-898b-e285baaf18c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adcc2546-5a4f-4dd1-b81b-480fea599e9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "510ff818-4456-4e6d-898b-e285baaf18c2",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "8c218bba-14e0-49aa-98b3-6b75b5e285eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "cf035421-99ab-4b9f-b7be-87e25ce4cefb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c218bba-14e0-49aa-98b3-6b75b5e285eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa22a493-ac1d-4492-92f4-e8c73d4a39ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c218bba-14e0-49aa-98b3-6b75b5e285eb",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "ee3fe0a9-fc80-4eb3-8620-f1224b2b205f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "d7224c0c-a8ec-4186-a53e-7741e822d037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee3fe0a9-fc80-4eb3-8620-f1224b2b205f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55ed348d-aeb9-4393-b4fc-efaba97c35d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee3fe0a9-fc80-4eb3-8620-f1224b2b205f",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "b7d5957e-a097-400c-b8df-5f6f41321236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "32ce9822-f129-45ba-a10f-a01f6f5f558b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7d5957e-a097-400c-b8df-5f6f41321236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3e52807-f55f-41d1-b887-2842f3c070a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d5957e-a097-400c-b8df-5f6f41321236",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "195ad188-67c8-4bbb-a24c-63841cd00eed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "9db2b9ad-dfb6-4a12-99f1-e950b745b3d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "195ad188-67c8-4bbb-a24c-63841cd00eed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b26f642-78ff-4ab0-9a49-402e4bf65d72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "195ad188-67c8-4bbb-a24c-63841cd00eed",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "b1068f04-b34e-4075-9fc8-d8e244dd1d17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "811fcdf4-5200-4cd9-a584-29033c04f7bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1068f04-b34e-4075-9fc8-d8e244dd1d17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74a84891-0370-4a20-95d5-e02a3d046924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1068f04-b34e-4075-9fc8-d8e244dd1d17",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "51c8d771-a158-4008-aeca-254507f1f506",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "b34bd112-a323-4184-af6e-d6dd0dab8a1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51c8d771-a158-4008-aeca-254507f1f506",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9afea28a-389e-4845-ab85-59be80048371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51c8d771-a158-4008-aeca-254507f1f506",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "b1063cf0-1395-4351-8915-49d9529fd177",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "e1329669-11a9-4b61-af0f-9bc0cc7a1496",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1063cf0-1395-4351-8915-49d9529fd177",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4df79b9e-fab6-4065-8665-4cb3c6ebb7fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1063cf0-1395-4351-8915-49d9529fd177",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "86eb5775-fc27-4424-936b-5017918f074e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "95fe62bc-d2a8-43d8-98dd-c61d3817e604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86eb5775-fc27-4424-936b-5017918f074e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43837e11-b213-4119-87dd-7998204ce030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86eb5775-fc27-4424-936b-5017918f074e",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        },
        {
            "id": "a2503889-de49-49b0-83ac-a0880044ab4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "compositeImage": {
                "id": "2f5c10af-8e55-4f45-a8f6-5c0fd0fa5da3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2503889-de49-49b0-83ac-a0880044ab4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3935510-e0b0-4eec-9995-9e86998bc657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2503889-de49-49b0-83ac-a0880044ab4d",
                    "LayerId": "21f0842b-7983-4b6c-a829-f0217e157a1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "21f0842b-7983-4b6c-a829-f0217e157a1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8dc238e-25c6-4650-82c4-804c09068287",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 20
}