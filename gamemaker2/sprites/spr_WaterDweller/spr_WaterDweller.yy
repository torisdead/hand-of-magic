{
    "id": "002bf388-5be3-4ffb-b066-11fde2fbe3e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WaterDweller",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9ddfe9c0-418b-4f48-9818-97d3af16cafa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "002bf388-5be3-4ffb-b066-11fde2fbe3e1",
            "compositeImage": {
                "id": "f592e2f4-7898-4a52-9768-0b1aa55fafa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ddfe9c0-418b-4f48-9818-97d3af16cafa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5833ecaf-13e3-4b91-a8fb-174e3ce1519c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ddfe9c0-418b-4f48-9818-97d3af16cafa",
                    "LayerId": "d110e517-773d-4ccc-8340-11701ad48d2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 66,
    "layers": [
        {
            "id": "d110e517-773d-4ccc-8340-11701ad48d2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "002bf388-5be3-4ffb-b066-11fde2fbe3e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 66,
    "xorig": -8,
    "yorig": 8
}