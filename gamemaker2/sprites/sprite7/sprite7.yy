{
    "id": "6f2b7c94-ae88-4f39-898f-14a55a2596ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 359,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4272727e-ea20-4ac2-b181-a44f92188aa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f2b7c94-ae88-4f39-898f-14a55a2596ed",
            "compositeImage": {
                "id": "da854a0c-8dc0-4c52-93cf-f24d5d877e9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4272727e-ea20-4ac2-b181-a44f92188aa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33d711c6-7e65-45e9-9336-ee2aefb3f162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4272727e-ea20-4ac2-b181-a44f92188aa6",
                    "LayerId": "50939303-ac50-42c5-98b7-aa579841f6af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "50939303-ac50-42c5-98b7-aa579841f6af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f2b7c94-ae88-4f39-898f-14a55a2596ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 360,
    "xorig": 0,
    "yorig": 0
}