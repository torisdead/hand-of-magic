{
    "id": "9994e788-0129-4ade-a90b-e9f44589550c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spikes_dungeon_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "30d775ed-8879-41bd-aa5c-2bb531323361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9994e788-0129-4ade-a90b-e9f44589550c",
            "compositeImage": {
                "id": "f97b964a-f931-40c9-a832-914f82cdb3a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30d775ed-8879-41bd-aa5c-2bb531323361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62716813-e6e3-49dd-b4d9-5396c1f6c385",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30d775ed-8879-41bd-aa5c-2bb531323361",
                    "LayerId": "ee1db884-8c70-4195-a652-27e72b498f31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ee1db884-8c70-4195-a652-27e72b498f31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9994e788-0129-4ade-a90b-e9f44589550c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}