{
    "id": "b60ed426-fdf0-4db2-b77a-8d84b1d1d85a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall001",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "be4b7d68-759f-4071-ab41-05f6e8254656",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b60ed426-fdf0-4db2-b77a-8d84b1d1d85a",
            "compositeImage": {
                "id": "c1d2f3b7-f738-4890-82fe-78bbf41efa08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be4b7d68-759f-4071-ab41-05f6e8254656",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "849fb20b-b40f-4716-bc9c-7a60f51752f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4b7d68-759f-4071-ab41-05f6e8254656",
                    "LayerId": "1356b237-669b-4c85-bfdb-3a67eee82b14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1356b237-669b-4c85-bfdb-3a67eee82b14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b60ed426-fdf0-4db2-b77a-8d84b1d1d85a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}