{
    "id": "071a92fa-6b8e-40b0-8123-2f49508bab13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite22",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "0ba19655-b7d0-4834-ad85-793a87c0eac7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "071a92fa-6b8e-40b0-8123-2f49508bab13",
            "compositeImage": {
                "id": "6ccdf7fc-e51a-4aa3-b5a0-d01c8d45acd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ba19655-b7d0-4834-ad85-793a87c0eac7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7158e64a-2401-415e-9622-7b8b81a898d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ba19655-b7d0-4834-ad85-793a87c0eac7",
                    "LayerId": "2c554719-379e-472b-8633-702ef1e4596e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2c554719-379e-472b-8633-702ef1e4596e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "071a92fa-6b8e-40b0-8123-2f49508bab13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}