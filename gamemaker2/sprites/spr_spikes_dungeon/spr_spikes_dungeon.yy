{
    "id": "c5500a78-06e9-4daf-9ec6-03c57ac25134",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spikes_dungeon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "35e8f012-2a52-4356-914a-4b889fe24fe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5500a78-06e9-4daf-9ec6-03c57ac25134",
            "compositeImage": {
                "id": "3ab7c0f5-6c6f-434f-b139-2ac60073c608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35e8f012-2a52-4356-914a-4b889fe24fe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7e09bc9-ffc0-4d13-87a9-5c1f0797dd56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35e8f012-2a52-4356-914a-4b889fe24fe6",
                    "LayerId": "70e9388e-2703-4dc2-85dc-e4762516e398"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "70e9388e-2703-4dc2-85dc-e4762516e398",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5500a78-06e9-4daf-9ec6-03c57ac25134",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}