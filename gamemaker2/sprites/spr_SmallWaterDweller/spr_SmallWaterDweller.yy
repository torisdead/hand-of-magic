{
    "id": "91595fae-10b2-4477-97ca-c6ddc7f50460",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_SmallWaterDweller",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "25705eb8-6410-4cba-b285-0fcdc45888c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91595fae-10b2-4477-97ca-c6ddc7f50460",
            "compositeImage": {
                "id": "7123976d-c1d7-4341-9cc8-6ccf59eccf63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25705eb8-6410-4cba-b285-0fcdc45888c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3e5dffc-8813-419b-92ea-6f223dbda2da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25705eb8-6410-4cba-b285-0fcdc45888c4",
                    "LayerId": "19ec2286-0397-4102-9e9a-91253e038182"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 66,
    "layers": [
        {
            "id": "19ec2286-0397-4102-9e9a-91253e038182",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91595fae-10b2-4477-97ca-c6ddc7f50460",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 66,
    "xorig": 0,
    "yorig": 0
}