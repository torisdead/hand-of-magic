{
    "id": "384d2b38-7e13-4a99-9695-f9731e74df44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GameOver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e0ec6b5c-bb0d-412b-8ccc-328d69ae4d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384d2b38-7e13-4a99-9695-f9731e74df44",
            "compositeImage": {
                "id": "24150dfa-447e-4614-a4b3-a9ffd2cf4cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0ec6b5c-bb0d-412b-8ccc-328d69ae4d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e2c6e9-06be-480c-b17c-a97f979856b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0ec6b5c-bb0d-412b-8ccc-328d69ae4d41",
                    "LayerId": "ec329e6b-6fa4-434c-aebf-980cd619b4d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "ec329e6b-6fa4-434c-aebf-980cd619b4d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "384d2b38-7e13-4a99-9695-f9731e74df44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 100
}