{
    "id": "1e77fc13-f99e-47a6-afe7-9e2fae2948fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "971823fc-64ed-44ca-bfe9-39fea94307ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e77fc13-f99e-47a6-afe7-9e2fae2948fb",
            "compositeImage": {
                "id": "1d6b6c52-7437-40ce-b0e0-60057dcd4bc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "971823fc-64ed-44ca-bfe9-39fea94307ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fef74e0-160e-4b7c-9ccb-3265fef8050f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "971823fc-64ed-44ca-bfe9-39fea94307ae",
                    "LayerId": "3b8934ec-dca0-4194-a76c-0353ea52fae9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3b8934ec-dca0-4194-a76c-0353ea52fae9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e77fc13-f99e-47a6-afe7-9e2fae2948fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}