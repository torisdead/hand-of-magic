{
    "id": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bigbit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "24569916-00e4-4449-92fa-6e623a676bc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "d19c4c62-a18b-4e59-94e3-cfd200323d96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24569916-00e4-4449-92fa-6e623a676bc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50959171-b714-4e96-a226-5a1f2e5eec6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24569916-00e4-4449-92fa-6e623a676bc8",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "ddeb32df-c45b-4946-a150-433da6e51f8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "db556477-204d-4f44-a90a-750e583cdbdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddeb32df-c45b-4946-a150-433da6e51f8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffb02ac6-fdea-4483-b987-4662646aa7c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddeb32df-c45b-4946-a150-433da6e51f8f",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "006d9d6c-37f9-4e51-8103-0c92edc7d9f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "d1922f9b-231d-4755-8fe5-0f27da9c77c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "006d9d6c-37f9-4e51-8103-0c92edc7d9f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23602e1a-94f1-4bef-8a51-3069fc9093a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "006d9d6c-37f9-4e51-8103-0c92edc7d9f8",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "1902e181-7ec7-42bc-9758-caf463e37c6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "d10f03c3-cb7d-400e-bd00-7a9675ed2cb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1902e181-7ec7-42bc-9758-caf463e37c6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "899b1e80-186a-436e-bdbb-d30933a970d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1902e181-7ec7-42bc-9758-caf463e37c6c",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "583bccd3-7a09-4b5b-a74e-c710a28f4dcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "35e8ea90-14d7-4ddf-b834-ec03cc89415d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "583bccd3-7a09-4b5b-a74e-c710a28f4dcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b3fcd19-9c8d-4bd1-8e1f-1bb57f4d13f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "583bccd3-7a09-4b5b-a74e-c710a28f4dcc",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "7ca935fc-08b3-4822-9dfc-8955c863992f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "8a8937c3-8b08-4176-be18-083ba1e935b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ca935fc-08b3-4822-9dfc-8955c863992f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54914721-1fbc-4e15-a6d4-7d9b39d8ae9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ca935fc-08b3-4822-9dfc-8955c863992f",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "9fca4c11-521f-44d2-aff6-9fb1973551bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "8dc3937e-58ed-4c59-954a-3fc8612be072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fca4c11-521f-44d2-aff6-9fb1973551bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93605687-9d32-458a-bacf-aa5e29946adf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fca4c11-521f-44d2-aff6-9fb1973551bb",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "6da37abf-8457-455b-979f-938bafff28b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "375ea53f-528a-44a6-8628-607a65a91120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6da37abf-8457-455b-979f-938bafff28b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26ee7485-3b26-4389-b039-7f2a200f54be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6da37abf-8457-455b-979f-938bafff28b0",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "5718b6a1-5e40-4a4e-972d-c65103245799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "ae081bf9-16b0-4af2-b0af-d00277a231a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5718b6a1-5e40-4a4e-972d-c65103245799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b78da24c-d63a-4044-8490-4fb7dc2a1db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5718b6a1-5e40-4a4e-972d-c65103245799",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "00c222a0-bfcd-4410-b3b0-b5375101ff26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "fa557e71-a7e8-458a-87ea-5d79926c4d3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c222a0-bfcd-4410-b3b0-b5375101ff26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5425727-b608-42dd-969c-b8778fa8f641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c222a0-bfcd-4410-b3b0-b5375101ff26",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "224d48b1-21fc-409b-9e82-0e77e2e6f6de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "9907accf-7f99-4518-910f-3136a11c0ab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "224d48b1-21fc-409b-9e82-0e77e2e6f6de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2282a62-08be-4556-9523-d40eef5ec350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "224d48b1-21fc-409b-9e82-0e77e2e6f6de",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "759d0fc1-2e01-4541-b74f-d21241295fff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "f702c4e7-7133-433f-8ec6-1622d5ec1b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "759d0fc1-2e01-4541-b74f-d21241295fff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe16169b-ab0c-4a2f-b671-8b205d186eda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "759d0fc1-2e01-4541-b74f-d21241295fff",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "f290a12b-b8ae-49ea-b2f4-e8f04eae1d7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "13e1e61e-4486-4b23-9802-68e32b709313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f290a12b-b8ae-49ea-b2f4-e8f04eae1d7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70674b07-02c2-4f76-a7d9-181ddd95aea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f290a12b-b8ae-49ea-b2f4-e8f04eae1d7a",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "f47f58be-e35c-4f2d-84d1-00c9a3763ddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "c066541a-8f2b-4181-8301-6a6637b6b046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f47f58be-e35c-4f2d-84d1-00c9a3763ddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce46eb6a-08e3-4ec0-ad1c-5b87714bb862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f47f58be-e35c-4f2d-84d1-00c9a3763ddf",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "deeb1536-6272-42b6-bf3e-0e0a751fd749",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "7a91a142-3577-4a64-a4dc-14b20d4a9261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deeb1536-6272-42b6-bf3e-0e0a751fd749",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b49b7cce-408d-4d0f-a026-f0e08e8f3ca3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deeb1536-6272-42b6-bf3e-0e0a751fd749",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "4bee3ce5-d44b-47cd-b311-4881a0ff3760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "31c78760-9bac-472f-a93e-edf3115b3038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bee3ce5-d44b-47cd-b311-4881a0ff3760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "092548a0-3854-4ca7-b59c-21997967fe2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bee3ce5-d44b-47cd-b311-4881a0ff3760",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "43a3f4ec-5eda-43b7-8de1-727a7f94188c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "2cf8d213-b06a-4d06-bf55-f560be5a6620",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43a3f4ec-5eda-43b7-8de1-727a7f94188c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d803adff-9a30-4625-8704-415e34980de7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43a3f4ec-5eda-43b7-8de1-727a7f94188c",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "45d3c4ad-e780-4616-a9f8-bd4917b5fd35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "c1517fd7-e4ec-49e1-b5ba-3978ae4f201e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d3c4ad-e780-4616-a9f8-bd4917b5fd35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "102bb185-e018-45d9-8547-6ce5245b7be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d3c4ad-e780-4616-a9f8-bd4917b5fd35",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "0443f0be-19dc-46d1-b585-90a22d893a0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "83428822-201c-4b8a-9149-c6b537801d91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0443f0be-19dc-46d1-b585-90a22d893a0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bed9812-0f04-4dc1-8ead-906e5b81bca0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0443f0be-19dc-46d1-b585-90a22d893a0a",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "10e5e25c-1a3f-422d-9548-dbaa87479669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "57b1872f-aeff-448f-9caa-dd21a7ec8938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10e5e25c-1a3f-422d-9548-dbaa87479669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fefd0a54-7c7b-492d-9488-d51486dfc383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10e5e25c-1a3f-422d-9548-dbaa87479669",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "9a7e6286-bc9b-463a-847d-935ff5b5c3f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "8d0d2307-59c0-4c22-b3f0-0013f19ec3ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a7e6286-bc9b-463a-847d-935ff5b5c3f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92a43fc1-a019-4f02-bea8-0363386723f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a7e6286-bc9b-463a-847d-935ff5b5c3f2",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "d436bbde-06c7-47ac-ba7d-165f0b3ce09b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "f2e1d87c-27e0-4149-9501-8db8e98b919e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d436bbde-06c7-47ac-ba7d-165f0b3ce09b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "672813dc-193e-48a6-b049-146aa3f007f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d436bbde-06c7-47ac-ba7d-165f0b3ce09b",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "5527a8df-fa38-46de-83a1-ea067116eb7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "5a08b018-e496-4b30-8e87-92592af0e755",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5527a8df-fa38-46de-83a1-ea067116eb7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "decc75f9-a537-47bb-8fde-25193675ac95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5527a8df-fa38-46de-83a1-ea067116eb7f",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "1c8e69ec-1d4a-4216-8ca6-a414849ef00a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "4e07b7c1-e20b-46e7-8dca-222ba8f60fbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c8e69ec-1d4a-4216-8ca6-a414849ef00a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "299ad9e6-b5ea-4574-984c-0ae9451a7103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c8e69ec-1d4a-4216-8ca6-a414849ef00a",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "5ed46bbc-100a-4e89-91e6-5c27a98ab04a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "b32bf344-7623-4593-99c8-ec0a330dc895",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ed46bbc-100a-4e89-91e6-5c27a98ab04a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b204d36-d72f-4ec3-ad51-611607e7a9e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ed46bbc-100a-4e89-91e6-5c27a98ab04a",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "0177076f-c2fc-4721-a6b8-22cc270784da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "f09756db-8ec5-4ce3-9ad0-ac2c9f61a6cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0177076f-c2fc-4721-a6b8-22cc270784da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a70412e-6baf-41a8-92cc-61958855fa62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0177076f-c2fc-4721-a6b8-22cc270784da",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "3b758bf1-faba-4f2f-970c-0985489f0b94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "df14c46c-a77e-4484-a752-fbb2700dbd09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b758bf1-faba-4f2f-970c-0985489f0b94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e91ee861-031c-4448-abf2-04159dde4a3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b758bf1-faba-4f2f-970c-0985489f0b94",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        },
        {
            "id": "16e6640d-12be-47f2-8bef-e9758621f0e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "compositeImage": {
                "id": "d467c425-d128-4f54-a3b5-29b966d685dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16e6640d-12be-47f2-8bef-e9758621f0e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4756108b-cbd0-41fa-8a7d-196837f964d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16e6640d-12be-47f2-8bef-e9758621f0e4",
                    "LayerId": "e994b588-efdc-4e40-b8c1-e2bbd261f10b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e994b588-efdc-4e40-b8c1-e2bbd261f10b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85a45444-54e1-4d54-bf72-8f1b71584b2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}