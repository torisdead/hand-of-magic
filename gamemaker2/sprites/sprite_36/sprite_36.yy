{
    "id": "12d7672f-47ee-43c9-aba2-671d5c4931d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_36",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d1ae92d7-c81d-46b7-891a-a464847bacbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12d7672f-47ee-43c9-aba2-671d5c4931d4",
            "compositeImage": {
                "id": "698817f6-2a66-4009-9923-29f33d6856d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1ae92d7-c81d-46b7-891a-a464847bacbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "346628cc-6332-4b1f-be07-6afa14e082df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1ae92d7-c81d-46b7-891a-a464847bacbf",
                    "LayerId": "1cb5ae5e-1c14-4816-a7de-cac18b302373"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1cb5ae5e-1c14-4816-a7de-cac18b302373",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12d7672f-47ee-43c9-aba2-671d5c4931d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}