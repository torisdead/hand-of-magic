{
    "id": "e7775784-87fb-4a9e-bd5d-c831fb8748ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgr_clouds",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c8d6aee4-3025-4b1c-8655-a38de93b0184",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7775784-87fb-4a9e-bd5d-c831fb8748ae",
            "compositeImage": {
                "id": "0e5f61ed-f654-4141-88a9-ea434adb0a26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8d6aee4-3025-4b1c-8655-a38de93b0184",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdf89984-5bd2-48bb-b842-aaaa161dc0a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8d6aee4-3025-4b1c-8655-a38de93b0184",
                    "LayerId": "15a1c111-a6e2-43df-80d0-9b9451b683a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "15a1c111-a6e2-43df-80d0-9b9451b683a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7775784-87fb-4a9e-bd5d-c831fb8748ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}