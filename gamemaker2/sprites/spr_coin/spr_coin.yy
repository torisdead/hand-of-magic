{
    "id": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c5a87a01-d5f7-4f5c-b729-7e26c5239e94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "1adbcddf-5546-4a96-96a4-61f69d337f73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a87a01-d5f7-4f5c-b729-7e26c5239e94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1678e87f-0e81-4ac6-a22e-c10c3c3b207b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a87a01-d5f7-4f5c-b729-7e26c5239e94",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "b912cbc5-a2ea-4c5a-89d3-9ab829cf86c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "d3c09707-4daa-475e-a2a8-2d66d5cdc4b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b912cbc5-a2ea-4c5a-89d3-9ab829cf86c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e0dc133-485e-4a20-8995-f6c6967344e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b912cbc5-a2ea-4c5a-89d3-9ab829cf86c8",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "b18bf857-cf47-4d67-9c70-42aa43e1de25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "1de67374-7cfb-4cc7-9237-3a2d80088b2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b18bf857-cf47-4d67-9c70-42aa43e1de25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb66e242-2b62-4c7f-99f6-1e2142e3db88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b18bf857-cf47-4d67-9c70-42aa43e1de25",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "910d122f-142d-454d-88a4-4e3bdbc938e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "53112e8b-2653-4a4a-9e3e-6992378c4b59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "910d122f-142d-454d-88a4-4e3bdbc938e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3372e3d8-c9cd-4aa4-aa25-ec0b888aa44e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "910d122f-142d-454d-88a4-4e3bdbc938e2",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "ba2d5db8-0ef3-40ff-ab0f-0cef46dafbd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "71e631ba-4e20-4ef3-9884-06ac5934775f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba2d5db8-0ef3-40ff-ab0f-0cef46dafbd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90b5a290-8b15-4e06-a608-aec4b3173d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba2d5db8-0ef3-40ff-ab0f-0cef46dafbd8",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "2dd7a8e6-59c1-4b63-9bfe-25b39b5adec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "9cb548b4-d8f5-455f-b540-e86372bdc672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dd7a8e6-59c1-4b63-9bfe-25b39b5adec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67efacf6-88fd-4a17-9af9-7de5361fff20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dd7a8e6-59c1-4b63-9bfe-25b39b5adec9",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "45d5faac-7317-4401-987e-d352f5ed2f2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "aa76afff-17cb-4566-baf6-e8364af22091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d5faac-7317-4401-987e-d352f5ed2f2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44a3ad4-32e0-4c59-81f8-9bc807daf9a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d5faac-7317-4401-987e-d352f5ed2f2f",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "eaa49334-9d8f-4dca-ba17-768a785590d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "b1144236-4947-4835-ae5f-69b99d5d5e51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaa49334-9d8f-4dca-ba17-768a785590d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e86f1de8-788c-4003-8d22-3e22b80e46df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaa49334-9d8f-4dca-ba17-768a785590d4",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "68ba3c88-91a3-4701-8a7a-d4f4132e6068",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "d66454c1-17f7-4387-b8be-da75eed4b979",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68ba3c88-91a3-4701-8a7a-d4f4132e6068",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4727ae26-1e46-4904-9c8a-1e2d505c902a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68ba3c88-91a3-4701-8a7a-d4f4132e6068",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "0488b58e-40fa-46da-a4af-cf066805d7dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "02c11b07-9a0e-4250-bff0-5a23bd282e89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0488b58e-40fa-46da-a4af-cf066805d7dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c81eae5-4243-4d16-86f6-5d5382f07954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0488b58e-40fa-46da-a4af-cf066805d7dd",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "d334d6ac-cdaa-47eb-a592-e948cb872137",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "4461a213-cea5-403e-aae7-f28f21c404ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d334d6ac-cdaa-47eb-a592-e948cb872137",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07ba9cd3-40c1-433c-b7dd-2774cd571a9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d334d6ac-cdaa-47eb-a592-e948cb872137",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "c60d30a5-f269-408f-bad9-4f12dd8b2aea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "66d8530e-d22b-4f7a-91ad-390bf56b333c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c60d30a5-f269-408f-bad9-4f12dd8b2aea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc91358-fa54-459f-a46e-f65eec0d5259",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c60d30a5-f269-408f-bad9-4f12dd8b2aea",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "ad2affa9-6946-4ac8-9355-2ada94eb911f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "8927b70c-0115-4b23-990a-c522cda724d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad2affa9-6946-4ac8-9355-2ada94eb911f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b01ea4b9-50ad-4768-a25a-b87e4b52f612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2affa9-6946-4ac8-9355-2ada94eb911f",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "86b54743-6033-475a-9b1c-a0cbce463f0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "4522ed2d-5ba7-411b-a57e-840da8463435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86b54743-6033-475a-9b1c-a0cbce463f0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "106b387f-bfb5-4217-aa32-a1ac3b57679c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86b54743-6033-475a-9b1c-a0cbce463f0e",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "a5ee5fb9-7534-4fcd-b4dc-3ec9e7ff4264",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "06eb319c-872f-4059-8cb3-87baca1209fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5ee5fb9-7534-4fcd-b4dc-3ec9e7ff4264",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d58f4458-29c7-4412-8daf-e8226900fc99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5ee5fb9-7534-4fcd-b4dc-3ec9e7ff4264",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "e6661db2-1a3a-4503-986e-d4119fd69ccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "558dd74d-2d45-4f66-b71f-95e71e2e882b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6661db2-1a3a-4503-986e-d4119fd69ccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f54180-7ce2-47d8-a220-052699463c21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6661db2-1a3a-4503-986e-d4119fd69ccd",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "166df2f3-935a-4e45-b1b9-18b9da6a9939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "49c1f820-2be7-4337-8c86-386dbfc24229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "166df2f3-935a-4e45-b1b9-18b9da6a9939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79457d8f-aa5e-48f2-a10f-f89a732afbb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "166df2f3-935a-4e45-b1b9-18b9da6a9939",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "bc6f361e-7805-489d-9637-a5259c62a45d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "d3ad255a-7ca3-4166-a64b-9c9dd4a86ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc6f361e-7805-489d-9637-a5259c62a45d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19a6aa2a-41f5-4eb9-8e6c-caa4985d9811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc6f361e-7805-489d-9637-a5259c62a45d",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "8ef437e2-aa8f-45df-b242-a69ffc8faea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "efc1426a-cee6-4544-a021-dbb2f32105f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ef437e2-aa8f-45df-b242-a69ffc8faea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3bfe871-2780-4a5b-810f-967480d03eab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ef437e2-aa8f-45df-b242-a69ffc8faea4",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "c7850b6b-7e93-4092-9f97-d028ae20bb89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "4e8434cb-7c96-48f7-ac8c-079c901a9320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7850b6b-7e93-4092-9f97-d028ae20bb89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0476e642-4f5a-465e-adb8-96bb327348c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7850b6b-7e93-4092-9f97-d028ae20bb89",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "4dc976a8-3e7a-4dd8-b8f3-7453850c9362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "2a8563bb-d0d9-42e5-85c4-de4bcf04e2a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dc976a8-3e7a-4dd8-b8f3-7453850c9362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87189744-3f22-4393-9657-4f9e3e811bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dc976a8-3e7a-4dd8-b8f3-7453850c9362",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "e131f4dd-b250-4b2d-ab8b-1e385c4ed435",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "5bcbc195-6f98-46d4-8f36-5b4b65cb816c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e131f4dd-b250-4b2d-ab8b-1e385c4ed435",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6183437c-d28b-499f-b795-60bbe138e64c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e131f4dd-b250-4b2d-ab8b-1e385c4ed435",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "5cc5db6f-5bb4-4fab-a78b-b607f0da917e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "e1276ea6-c9fe-4e3f-a580-616829a94c47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cc5db6f-5bb4-4fab-a78b-b607f0da917e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b97241-204e-42bb-abf8-63d87c327835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cc5db6f-5bb4-4fab-a78b-b607f0da917e",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "7a83aee6-ee4f-4d2d-b5b8-a962c5b02f32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "b48c6c4e-0a11-40f2-8ed6-6d40d3881228",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a83aee6-ee4f-4d2d-b5b8-a962c5b02f32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5865fa25-9657-4edc-a5fc-99f2c836cc7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a83aee6-ee4f-4d2d-b5b8-a962c5b02f32",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "986dfa93-3013-41b9-945c-d03948da4de6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "c4afc288-c5ee-4e46-8ac2-343e78a96489",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "986dfa93-3013-41b9-945c-d03948da4de6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf194daa-efcd-4dbc-b61c-588a6ae26693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "986dfa93-3013-41b9-945c-d03948da4de6",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "0196539c-bd8d-4415-9e01-8a7868706ed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "64729fe9-8e58-419a-beda-5c662f661fc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0196539c-bd8d-4415-9e01-8a7868706ed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b93ff2d0-52af-495f-8fab-5db90edfd97e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0196539c-bd8d-4415-9e01-8a7868706ed1",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "84b88674-279a-41f2-99e5-faa45e42cf14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "371cb7fe-ce55-4d6e-a537-7d9e37e5323e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b88674-279a-41f2-99e5-faa45e42cf14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb91df2c-aae6-4346-aa0e-e712faa64fb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b88674-279a-41f2-99e5-faa45e42cf14",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        },
        {
            "id": "56df979c-c16a-4e3e-a985-06e1e5963c1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "compositeImage": {
                "id": "a3fcc7bc-fe5f-448c-b184-ea912eed104f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56df979c-c16a-4e3e-a985-06e1e5963c1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76fbf3fd-9f54-4805-949f-a4992c170be3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56df979c-c16a-4e3e-a985-06e1e5963c1e",
                    "LayerId": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d32c160a-ad6e-4bb8-bf3c-31cb9ad1adc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72672f7e-43cd-44fe-aa5e-9b9d0a894cae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}