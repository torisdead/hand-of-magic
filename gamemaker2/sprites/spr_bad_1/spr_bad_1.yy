{
    "id": "a3d7bed6-c6b9-4e44-95c2-e0ae4f515aec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bad_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 2,
    "bbox_right": 61,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3790846a-e51b-4bc3-ba57-fed5c05012e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3d7bed6-c6b9-4e44-95c2-e0ae4f515aec",
            "compositeImage": {
                "id": "f5ad323d-5b31-4104-ae59-1ab895adf832",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3790846a-e51b-4bc3-ba57-fed5c05012e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8abd85bb-8353-45f1-ac57-8d26b933d108",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3790846a-e51b-4bc3-ba57-fed5c05012e8",
                    "LayerId": "9f160717-464b-461a-8f15-128f93ac8795"
                }
            ]
        },
        {
            "id": "2bc8b3c1-fea1-4a9d-8e5c-31dd006c39fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3d7bed6-c6b9-4e44-95c2-e0ae4f515aec",
            "compositeImage": {
                "id": "e2d02259-1b1f-4a80-af2e-6304c6c0f246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc8b3c1-fea1-4a9d-8e5c-31dd006c39fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "291714e7-b4d6-4eeb-aa34-64f355da294a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc8b3c1-fea1-4a9d-8e5c-31dd006c39fb",
                    "LayerId": "9f160717-464b-461a-8f15-128f93ac8795"
                }
            ]
        },
        {
            "id": "fb9656ca-6cec-47a5-b0d2-0680413bf415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3d7bed6-c6b9-4e44-95c2-e0ae4f515aec",
            "compositeImage": {
                "id": "ea0d3325-45d6-4123-be83-4b3afe4f7766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb9656ca-6cec-47a5-b0d2-0680413bf415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7b67b45-5e70-43c1-90e0-a685e3d494d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb9656ca-6cec-47a5-b0d2-0680413bf415",
                    "LayerId": "9f160717-464b-461a-8f15-128f93ac8795"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9f160717-464b-461a-8f15-128f93ac8795",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3d7bed6-c6b9-4e44-95c2-e0ae4f515aec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}