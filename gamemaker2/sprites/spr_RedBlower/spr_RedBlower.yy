{
    "id": "3dded49c-5657-4d1b-ae0e-e09b76dd7776",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_RedBlower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "8b8b7260-e4e5-49f9-a420-ebdbc5c58a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dded49c-5657-4d1b-ae0e-e09b76dd7776",
            "compositeImage": {
                "id": "464a8253-e241-4735-8d63-70187178811c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b8b7260-e4e5-49f9-a420-ebdbc5c58a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30ae2f02-fd9a-4178-8997-7d82365f9648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b8b7260-e4e5-49f9-a420-ebdbc5c58a0c",
                    "LayerId": "6b05f78e-66a6-4bae-b135-fce77fa911be"
                }
            ]
        },
        {
            "id": "a44cfadb-4919-46b4-8fbe-674b02f0b6a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dded49c-5657-4d1b-ae0e-e09b76dd7776",
            "compositeImage": {
                "id": "a560d400-a815-4574-b365-a366c619af2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a44cfadb-4919-46b4-8fbe-674b02f0b6a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98407554-0dc4-4895-b541-8d4cf955072b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a44cfadb-4919-46b4-8fbe-674b02f0b6a9",
                    "LayerId": "6b05f78e-66a6-4bae-b135-fce77fa911be"
                }
            ]
        },
        {
            "id": "20bdcacc-616b-4949-b690-2b1147c07383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dded49c-5657-4d1b-ae0e-e09b76dd7776",
            "compositeImage": {
                "id": "43f11230-6699-48a6-a2b4-a6d8a767cba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20bdcacc-616b-4949-b690-2b1147c07383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2388d2f3-bfdc-4f31-8b67-9accbbd1efde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20bdcacc-616b-4949-b690-2b1147c07383",
                    "LayerId": "6b05f78e-66a6-4bae-b135-fce77fa911be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6b05f78e-66a6-4bae-b135-fce77fa911be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dded49c-5657-4d1b-ae0e-e09b76dd7776",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}