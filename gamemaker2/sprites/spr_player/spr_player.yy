{
    "id": "cf81516f-4b11-4874-aa1b-8e719340628d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f8643e03-2fc8-4d04-b42b-b9152ab4ed5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf81516f-4b11-4874-aa1b-8e719340628d",
            "compositeImage": {
                "id": "dcc561a7-091b-45f8-9691-20e9e2f7df9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8643e03-2fc8-4d04-b42b-b9152ab4ed5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb421720-1085-47cb-a2cf-c4554e8d6b3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8643e03-2fc8-4d04-b42b-b9152ab4ed5a",
                    "LayerId": "9fb174da-e1dd-4e71-949c-0473b5eb3d1a"
                },
                {
                    "id": "4675967c-29af-4c97-9f16-8abf8d161cc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8643e03-2fc8-4d04-b42b-b9152ab4ed5a",
                    "LayerId": "783d1376-2d04-47c3-ad0a-01e3bbe98f67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 66,
    "layers": [
        {
            "id": "783d1376-2d04-47c3-ad0a-01e3bbe98f67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf81516f-4b11-4874-aa1b-8e719340628d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9fb174da-e1dd-4e71-949c-0473b5eb3d1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf81516f-4b11-4874-aa1b-8e719340628d",
            "blendMode": 0,
            "isLocked": true,
            "name": "Background",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 66,
    "xorig": 33,
    "yorig": 33
}