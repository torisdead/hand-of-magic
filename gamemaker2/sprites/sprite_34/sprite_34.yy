{
    "id": "bb19c553-41ff-49f0-bd94-feddc07bd350",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_34",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "49d5da05-a404-4f58-aeab-8eea4837dfa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb19c553-41ff-49f0-bd94-feddc07bd350",
            "compositeImage": {
                "id": "2d023e7b-6ac2-4964-b034-140828bd623f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49d5da05-a404-4f58-aeab-8eea4837dfa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e7be18d-d03d-4d44-8dc1-26c58b8e0051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49d5da05-a404-4f58-aeab-8eea4837dfa8",
                    "LayerId": "d754ddad-bc65-4a5f-bff2-9d9298d42f2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d754ddad-bc65-4a5f-bff2-9d9298d42f2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb19c553-41ff-49f0-bd94-feddc07bd350",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}