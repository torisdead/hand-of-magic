{
    "id": "8d16667f-3b90-41db-a335-67fb6d99fea6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PurpleMoth",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "eac76feb-d891-4dd7-b268-117975451477",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d16667f-3b90-41db-a335-67fb6d99fea6",
            "compositeImage": {
                "id": "59690f21-ff6d-4bf3-a4b2-bd116fc3781a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eac76feb-d891-4dd7-b268-117975451477",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e79d5da9-c56f-4d93-806c-6e1a029921ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eac76feb-d891-4dd7-b268-117975451477",
                    "LayerId": "c9b0f372-fffb-40c7-a897-2d8bbff4920f"
                }
            ]
        },
        {
            "id": "8266aa70-a45d-433e-85ac-9ea15953bae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d16667f-3b90-41db-a335-67fb6d99fea6",
            "compositeImage": {
                "id": "3036eb67-228c-4125-99c4-f71e373c0f59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8266aa70-a45d-433e-85ac-9ea15953bae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b11e8215-8568-49cc-9af3-e37878150176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8266aa70-a45d-433e-85ac-9ea15953bae3",
                    "LayerId": "c9b0f372-fffb-40c7-a897-2d8bbff4920f"
                }
            ]
        },
        {
            "id": "d5770ac6-e327-40ca-8388-7bb3f5cf0ae1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d16667f-3b90-41db-a335-67fb6d99fea6",
            "compositeImage": {
                "id": "becf8054-923c-4bbc-9f43-b053f51f2c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5770ac6-e327-40ca-8388-7bb3f5cf0ae1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61f15049-8807-4628-9e6f-cfb457d51786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5770ac6-e327-40ca-8388-7bb3f5cf0ae1",
                    "LayerId": "c9b0f372-fffb-40c7-a897-2d8bbff4920f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c9b0f372-fffb-40c7-a897-2d8bbff4920f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d16667f-3b90-41db-a335-67fb6d99fea6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}