{
    "id": "f0d4f55c-aec6-412b-93dc-11cc2c9388e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bad_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1bcf03a8-1012-47a9-bf57-334cfbd75dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0d4f55c-aec6-412b-93dc-11cc2c9388e0",
            "compositeImage": {
                "id": "1d825f0d-7318-4066-a6fd-5282d9934fea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bcf03a8-1012-47a9-bf57-334cfbd75dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01f7fbc8-2877-4bcd-b3a5-50e0e4273b8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bcf03a8-1012-47a9-bf57-334cfbd75dbd",
                    "LayerId": "2076ae1f-e392-45a9-a76d-4f879346e8a0"
                }
            ]
        },
        {
            "id": "85f23b4a-2424-4eba-82fd-be232960b784",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0d4f55c-aec6-412b-93dc-11cc2c9388e0",
            "compositeImage": {
                "id": "6d792ddb-b33f-4a9a-b3a3-0bc3b1f4d967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85f23b4a-2424-4eba-82fd-be232960b784",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b97543be-2fc8-4f7f-81cc-9ae4806f513d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85f23b4a-2424-4eba-82fd-be232960b784",
                    "LayerId": "2076ae1f-e392-45a9-a76d-4f879346e8a0"
                }
            ]
        },
        {
            "id": "5728d242-c55c-41c2-b070-3d5070b99a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0d4f55c-aec6-412b-93dc-11cc2c9388e0",
            "compositeImage": {
                "id": "58269d33-340e-4af0-b3e9-81b86d8466b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5728d242-c55c-41c2-b070-3d5070b99a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df32e1de-054d-41c8-bb4a-c144fbe3ba72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5728d242-c55c-41c2-b070-3d5070b99a12",
                    "LayerId": "2076ae1f-e392-45a9-a76d-4f879346e8a0"
                }
            ]
        },
        {
            "id": "935da854-c44f-4437-a894-d5e4ff262644",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0d4f55c-aec6-412b-93dc-11cc2c9388e0",
            "compositeImage": {
                "id": "b9fdea37-250d-4e90-8af0-71a6b22b4216",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "935da854-c44f-4437-a894-d5e4ff262644",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c376673-52e8-4203-9fa4-468f194dfe60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "935da854-c44f-4437-a894-d5e4ff262644",
                    "LayerId": "2076ae1f-e392-45a9-a76d-4f879346e8a0"
                }
            ]
        },
        {
            "id": "417038f2-15b9-44de-a21c-13a20d16e406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0d4f55c-aec6-412b-93dc-11cc2c9388e0",
            "compositeImage": {
                "id": "013a8497-1a6b-4e30-9969-4cd393781f61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417038f2-15b9-44de-a21c-13a20d16e406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c365f521-adb9-4b1f-a026-1916a738d9fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417038f2-15b9-44de-a21c-13a20d16e406",
                    "LayerId": "2076ae1f-e392-45a9-a76d-4f879346e8a0"
                }
            ]
        },
        {
            "id": "5357a71f-4931-42c7-91ba-aacffa7985ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0d4f55c-aec6-412b-93dc-11cc2c9388e0",
            "compositeImage": {
                "id": "64f491d5-f472-4357-989e-0c441b3a8c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5357a71f-4931-42c7-91ba-aacffa7985ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00da5887-4906-4047-8797-4983cd6dab43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5357a71f-4931-42c7-91ba-aacffa7985ac",
                    "LayerId": "2076ae1f-e392-45a9-a76d-4f879346e8a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2076ae1f-e392-45a9-a76d-4f879346e8a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0d4f55c-aec6-412b-93dc-11cc2c9388e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 0.1,
    "playbackSpeedType": 1,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}